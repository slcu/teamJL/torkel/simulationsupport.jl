#Steady State extensions
function DiffEqBiological.steady_states(model::DiffEqParameters.AbstractParameter{Float64},args...;kwargs...)
    steady_states(model.model,model.param,args...;kwargs...)
end
function DiffEqBiological.bifurcations(model::DiffEqParameters.AbstractParameter{Float64},args...;kwargs...)
   bifurcations(model.model,model.param,args...;kwargs...)
end
function DiffEqBiological.bifurcation_grid(model::DiffEqParameters.AbstractParameter{Float64},args...;kwargs...)
   bifurcation_grid(model.model,model.param,args...;kwargs...)
end
function DiffEqBiological.bifurcation_grid_2d(model::DiffEqParameters.AbstractParameter{Float64},args...;kwargs...)
   bifurcation_grid_2d(model.model,model.param,args...;kwargs...)
end
function DiffEqBiological.bifurcation_grid_diagram(model::DiffEqParameters.AbstractParameter{Float64},args...;kwargs...)
   bifurcation_grid_diagram(model.model,model.param,args...;kwargs...)
end

#Find initial conditions.
function makeu0(model, u0,starting_steady_state_number=1; fixed_conc...)
    (length(u0)!=0) && (return u0)
    ss = steady_states(model)
    #(length(ss)>1) && @warn "Initial condition have several possible steady states, uses the first one"
    return sort(ss)[starting_steady_state_number]
end
#Finds the tstops for a given callback vector.
function find_tstops(p_steps,v_steps)
    first.(filter(i->typeof(i)!=Symbol,[p_steps...,v_steps...]))
end

#Makes a deterministic simulation.
function detsim(model, tspan; u0=[], solver=Rosenbrock23(), p_steps=(), v_steps=(), kwargs...)
    prob = ODEProblem(model.model,makeu0(model,u0),tspan,deepcopy(model))
    return DifferentialEquations.solve(prob,solver;callback=CallbackSet(par_steps(p_steps)...,var_steps(v_steps)...),tstops=find_tstops(p_steps,v_steps), kwargs...)
end

#Makes a stochastic simulation.
function stochsim(model, tspan; u0=[], starting_steady_state_number=1, solver=ImplicitEM(), p_steps=(), v_steps=(), kwargs...)
    prob = SDEProblem(model.model,makeu0(model,u0,starting_steady_state_number),tspan,deepcopy(model))
    return DifferentialEquations.solve(prob,solver;callback=CallbackSet(positive_domain(),par_steps(p_steps)...,var_steps(v_steps)...),tstops=find_tstops(p_steps,v_steps), kwargs...)
end

#Makes several stochastic simulations.
function monte(model, tspan, n; u0=[], starting_steady_state_number=1, eSolver=EnsembleThreads(), solver=ImplicitEM(), p_steps=(), v_steps=(), kwargs...)
    prob = SDEProblem(model.model,makeu0(model,u0,starting_steady_state_number),tspan,deepcopy(model))
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->p)
    return solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(positive_domain(),par_steps(p_steps)...,var_steps(v_steps)...),tstops=find_tstops(p_steps,v_steps), kwargs...)
end

#Makes various SSA simulations (needs fixing).
function ssa_equi(model; fixed_conc...)
    syms = model.model.syms; uInit = fill(10, length(syms));
    foreach(i -> (haskey(fixed_conc,syms[i])&&(uInit[i]=fixed_conc[syms[i]])), 1:length(syms))
    prob = JumpProblem(DiscreteProblem(uInit,(0,10.),model),Direct(),model.model)
    sol = DifferentialEquations.solve(prob,SSAStepper());
    return sol[end]
end
function ssasim(model, tspan; solver=FunctionMap(),p_steps=(),v_steps=(),saveat=0.1,fixed_conc...)
    u0 = ssa_equi(model;fixed_conc...)
    prob = JumpProblem(DiscreteProblem(u0,tspan,deepcopy(model)),Direct(),model.model)
    return DifferentialEquations.solve(prob,solver,callback=CallbackSet(par_steps(p_steps)...,var_steps(v_steps)...),tstops=find_tstops(p_steps,v_steps), saveat=saveat)
end

function ssa_monte(model, tspan, n; eSolver=EnsembleThreads(), solver=FunctionMap(), p_steps=(), v_steps=(), saveat=0.1, fixed_conc...)
    u0 = ssa_equi(model;fixed_conc...)
    prob = JumpProblem(DiscreteProblem(u0,tspan,deepcopy(model)),Direct(),model.model)
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->p)
    return solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(par_steps(p_steps)...,var_steps(v_steps)...),tstops=find_tstops(p_steps,v_steps), saveat=saveat)
end

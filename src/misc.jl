#Implements the Gamma distrivuted Ornstein Uhlenbeck process. Adapted from James Locke.
function makeOUgamma(x, λ, ν, α, T, N)
    Δ = T/N
    pathz = zeros(N+1)
    pathgou = zeros(N+1)

    pathz[1] = 0
    pathgou[1] = x

    for i = 2:(N+1)
        getnext = generateGAMOU(Δ,λ,ν,α)
        pathz[i] = pathz[i-1]+getnext[1]
        pathgou[i] = exp(-λ*Δ)*pathgou[i-1]+getnext[2]
    end
    GOU = (pathz, pathgou)
end
function generateGAMOU(Δ,λ,ν,α)
    pse = rand(Exponential(1/(λ*ν)))
    zpse = 0;
    xpse = 0;
    while (pse <= Δ)
        y = rand(Exponential(1/α))
        zpse = zpse+y;
        xpse = xpse+exp(-λ*(Δ-pse))*y;
        pse = pse+rand(Exponential(1/(λ*ν)));
    end
    getnext = (zpse,xpse);
end

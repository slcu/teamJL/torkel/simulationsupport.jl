using DiffEqBase

# The internal structure for a set of solutions, conatining only the minimal information.
mutable struct SimpleSimulationData
    t::Vector{Float64}      # The vector with all the time values (should be the same for all simulations).
    u::Array{Float64,3}     # A matrix with all simulation values.
    m::Int64                # The number of simulations (if not a monte-carlo simulation, m=1).
    l::Int64                # The number of time-steps in each simulation (= length(t))
    n::Int64                # The number if varriables in each simulation.

    SimpleSimulationData(t,u,m,l,n) = new(t,u,m,l,n) # Makes the structure from the field values.
    function SimpleSimulationData(sols::DiffEqBase.AbstractEnsembleSolution) # Makes the structure from a monte carlo simulation.
        l = maximum(length.(getfield.(sols[:],:t)))
        sols_correct = findall(length.(getfield.(sols[:],:t)) .== l)
        m,n = length.([sols_correct,sols[1].u[1]])
        (m!= length(sols)) && @warn "A total of "*string(m-length(sols))*" solutions were discarded due to having an incomplete t vector."
        u = Array{Float64,3}(undef,m,l,n)
        for M = 1:m, L = 1:l, N = 1:n
            u[M,L,N] = sols[sols_correct[M]].u[L][N]
        end
        new(deepcopy(sols[sols_correct[1]].t),u,m,l,n)
    end
    function SimpleSimulationData(sol::DiffEqBase.AbstractODESolution) # Makes the structure from a single simulation.
        m,l,n = length.((1,sol.t,sol.u[1]))
        u = Array{Float64,3}(undef,1,l,n)
        for L = 1:l, N = 1:n
            u[1,L,N] = sol.u[L][N]
        end
        new(deepcopy(sol.t),u,m,l,n)
    end
    function SimpleSimulationData(sols1::SimpleSimulationData, sols2::SimpleSimulationData) # Combines two structures to a single one.
        (sols1.l != sols2.l) && error("The two solution sets must be of equal length.")
        !all((sols1.t) .== (sols2.t)) && error("The two solution sets must have identical t vectors.")
        (sols1.n != sols2.n) && error("The two solution sets must have the same number of reactants.")
        m,l,n = (sols1.m+sols2.m,sols1.l,sols1.n)
        u = Array{Float64,3}(undef,m,l,n)
        for L = 1:l, N = 1:n
            foreach(M -> u[M,L,N] = sols1.u[M,L,N], 1:sols1.m)
            foreach(M -> u[sols1.m+M,L,N] = sols2.u[M,L,N], 1:sols2.m)
        end
        new(sols1.t,u,sols1.m+sols2.m,sols1.l,sols1.n)
    end
end

# Saves a solution set to a csv file.
# First line contains all time points.
# Next each line contains one solution. It contains one field for solution at each time point (fields separeted by ',').
# Each field contains the values of each variable in order (varriable values separeted ' ').
function save_simulations(sols::SimpleSimulationData,filename)
    io = open(filename*".csv", "w")
    foreach(L -> print(io, sols.t[L], ","), 1:sols.l-1)
    print(io, sols.t[end], "\n")
    for M = 1:sols.m
        for L = 1:sols.l
            foreach(N -> print(io, sols.u[M,L,N], " "), 1:sols.n-1)
            print(io, sols.u[M,L,end])
            (L==sols.l) ? print(io, "\n") : print(io, ",")
        end
    end
    close(io)
end
save_simulations(sols::DiffEqBase.AbstractEnsembleSolution,name) = save_simulations(SimpleSimulationData(sols),name)
save_simulations(sol::DiffEqBase.AbstractODESolution,name) = save_simulations(SimpleSimulationData(sol),name)

# save_simulations(), but with and upper threshold of how many simulations are saved.
save_simulations(sols::DiffEqBase.AbstractEnsembleSolution,name,thres_saves::Int64) = save_simulations(SimpleSimulationData(sols),name,thres_saves)
save_simulations(sol::DiffEqBase.AbstractODESolution,name,thres_saves::Int64) = save_simulations(SimpleSimulationData(sol),name,thres_saves)
function save_simulations(sols::SimpleSimulationData,filename,thres_saves::Int64)
    save_simulations(SimpleSimulationData(sols.t,sols.u[1:min(sols.m,thres_saves),:,:],min(sols.m,thres_saves),sols.l,sols.n),filename)
end

# Saves a solution, but if the file already exists, adds the solutions to it instead.
function additive_save_simulations(sols_input,filename)
    sols = (typeof(sols_input)==SimpleSimulationData) ? sols_input : SimpleSimulationData(sols_input)
    if isfile(filename*".csv")
        save_simulations(SimpleSimulationData(sols,load_simulations(filename)),filename)
    else
        save_simulations(sols,filename)
    end
end

# Resaves a set of simulations, but with only a single varriable.
function resave_simulations(filename,save_suffix,var)
    save_simulations(load_simulations(filename,var),filename*save_suffix)
end

#Loads the solution from a file.
function load_simulations(filename)
    file_txt = readlines(filename*".csv")
    t = map(entry->parse(Float64,entry),split(file_txt[1],","))
    m = length(file_txt)-1
    l = length(t)
    n = length(split(split(file_txt[2],",")[1]," "))
    sols = Array{Float64,3}(undef,m,l,n)
    for M = 1:m
        sol = split(file_txt[M+1],",")
        foreach(L -> sols[M,L,:] = parse.(Float64,split(sol[L]," ")), 1:l)
    end
    return SimpleSimulationData(t,sols,m,l,n)
end

#Loads a single varriable from saved simulations.
function load_simulations(filename,var)
    sims = load_simulations(filename)
    return SimpleSimulationData(sims.t,sims.u[:,:,var:var],sims.m,sims.l,1)
end

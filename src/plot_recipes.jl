@recipe function f(data::SimpleSimulationData; var=1, xguide="Time", label="", linealpha=0.6)
    linealpha --> linealpha
    xguide   --> xguide
    label   --> label
    data.t,data.u[:,:,var]'
end

@recipe function f(data::SimpleSimulationData, sol::Int64; var=1, xguide="Time", label="", linealpha=0.6)
    linealpha --> linealpha
    xguide   --> xguide
    label   --> label
    data.t,data.u[sol,:,var]
end
